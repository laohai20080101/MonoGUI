// OAccell.h: interface for the OAccell class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__OACCELL_H__)
#define __OACCELL_H__

typedef struct _ACCELL
{
	int    nKeyValue;
	int    id;
	struct _ACCELL* next;
} ACCELL;


class OAccell
{
private:
	int     m_nCount;
	ACCELL* m_pAccell;

public:
	OAccell();
	virtual ~OAccell();

	// 初始化快捷键列表
	BOOL Create (char* sAccellTable, KeyMap* pKeyMap);

	// 搜索快捷键列表
	int Find (int nKeyValue);

private:
	// 删除所有快捷键表项
	BOOL RemoveAll ();
};

#endif // !defined(__OACCELL_H__)
