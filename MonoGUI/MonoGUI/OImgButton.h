// OImgButton.h: interface for the OImgButton class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__OIMAGEBUTTON_H__)
#define __OIMAGEBUTTON_H__


class OImgButton : public OWindow
{
private:
	enum { self_type = WND_TYPE_IMAGE_BUTTON };

public:

	// 定义图像按钮所需要的图片
	// 从上到下分别是：
	// 1>Normal样式的图片
	// 2>Focus样式的图片
	// 3>PushDown样式的图片
	BW_IMAGE* m_pImage;

	OImgButton ();
	virtual ~OImgButton ();

	BOOL Create (OWindow* pParent,
		WORD wStyle,
		WORD wStatus,
		int x,
		int y,
		int w,
		int h,
		int ID
	);
	
	// 虚函数，绘制按钮
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

#if defined (MOUSE_SUPPORT)
	// 坐标设备消息处理
	virtual int PtProc (OWindow* pWnd, int nMsg, int wParam, int lParam);
#endif // defined(MOUSE_SUPPORT)

	// 调入图片
	BOOL SetImage (BW_IMAGE* pImage);

private:
	// 按键被按下的处理
	void OnPushDown ();
};

#endif // !defined(__OIMGBUTTON_H__)
