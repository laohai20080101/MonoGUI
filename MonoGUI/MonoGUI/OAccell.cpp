// OAccell.cpp: implementation of the OAccell class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
OAccell::OAccell()
{
	m_nCount = 0;
	m_pAccell = NULL;
}

OAccell::~OAccell()
{
	// 删除快捷键列表
	RemoveAll ();
}

// 初始化快捷键列表
BOOL OAccell::Create (char* sAccellTable, KeyMap* pKeyMap)
{
	RemoveAll ();
	char sKeyName[256];
	char sTemp[256];

	ACCELL* theNext = m_pAccell;

	int k = 0;
	BOOL bFirst = TRUE;

	while (1)
	{
		memset (sKeyName, 0x0, sizeof(sKeyName));
		if (! GetSlice (sAccellTable, sKeyName, k))
		{
			return TRUE;
		}

		int nKeyValue = pKeyMap->Find (sKeyName);
		if (this->Find (nKeyValue) != -1) {
			return FALSE;	// 该KeyValue已经存在，不能再添加了
		}
	    
		k ++;

		memset (sTemp, 0x0, sizeof(sTemp));
		if (! GetSlice (sAccellTable, sTemp, k))
		{
			return FALSE;	// 不成组
		}

		k ++;

		int nButtonID = atoi(sTemp);

		m_nCount ++;
		ACCELL* theNewOne = new ACCELL;
		theNewOne->nKeyValue = nKeyValue;
		theNewOne->id = nButtonID;

		if (bFirst)	// 如果是第一次，新建表项的指针赋给m_pAccell
		{
			m_pAccell = theNewOne;
			theNext = m_pAccell;
			bFirst = FALSE;
		}
		else
		{
			theNext->next = theNewOne;
			theNext = theNewOne;
		}
	}
}

// 搜索快捷键列表
int OAccell::Find (int nKeyValue)
{
	ACCELL* theCurrent = m_pAccell;
	
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (theCurrent->nKeyValue == nKeyValue)
		{
		    return theCurrent->id;
		}
		theCurrent = theCurrent->next;
	}
	return -1;
}

BOOL OAccell::RemoveAll ()
{
	ACCELL* theNext = NULL;

	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_pAccell == NULL)
		{
			m_nCount = 0;
			return FALSE;
		}

		theNext = m_pAccell->next;
		if (m_pAccell != NULL)
		{
			delete m_pAccell;
		}
		m_pAccell = theNext;
	}
	m_nCount = 0;
	return TRUE;
}

/* END */
