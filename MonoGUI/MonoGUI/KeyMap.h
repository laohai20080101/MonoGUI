// KeyMap.h
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__KEYMAP_H__)
#define __KEYMAP_H__

#if !defined(BOOL)
#define BOOL int
#endif // !defined(BOOL)

typedef struct _KEYMAP
{
	char sKeyName[32];
	int  nKeyValue;
} KEYMAP;

#define KEY_MAX 64

class KeyMap
{
private:
	int m_nCount;
	KEYMAP m_pstKeyMap[KEY_MAX];

public:
	KeyMap();
	virtual ~KeyMap();

public:
	BOOL Load (char* sFileName);
	int  Find (char* sKeyName);
	int  GetCount ();
	BOOL GetName (int nIndex, char* sKeyName);
};

#endif // !defined(__KEYMAP_H__)
